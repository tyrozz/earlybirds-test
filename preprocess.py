from __future__ import print_function, division
from builtins import range, input

import pandas as pd
df_eval = pd.read_csv('evaluation_ratings.csv')
df_movies = pd.read_csv('movies_metadata.csv')

df = pd.read_csv('ratings.csv')

# make the user ids go from 0...N-1
df.userId = df.userId - 1

# create a mapping for movie ids
unique_movie_ids = set(df.movieId.values)
movie2idx = {}
count = 0
for movie_id in unique_movie_ids:
  movie2idx[movie_id] = count
  count += 1
    
# add them to the data frame
# takes awhile
df['movie_idx'] = df.apply(lambda row: movie2idx[row.movieId], axis=1)

df = df.drop(['timestamp'], axis=1)

df.to_csv('edited_rating.csv', index=False)