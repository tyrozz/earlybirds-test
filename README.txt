The order to run the files:

1. preprocess.py

2. preprocess_shrink.py

3. preprocess2dict.py

Then userbased.py or itembased.py


I worked with a small set of the data. So the first three programs deal with the data preparation and shrinking the data. 

userbased.py is the implementation of user-user collaborative filtering algorithm and  itembased.py is the implementation of item-item collaborative filtering.
